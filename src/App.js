import React, {useState, useEffect} from 'react';
import { Form } from 'react-bootstrap';
import TodoItem from './components/TodoItem'

import './App.scss';

const App = () => {
  const [data, setData] = useState(JSON.parse(localStorage.getItem("todo-app")) || []);
  const [edit, setEdit] = useState(false);
  const [task, setTask] = useState("");

  useEffect(() => {
    localStorage.setItem('todo-app', JSON.stringify(data));
    console.log(data);
  }, [data]);

  const handleSubmit = (event) => {
    event.preventDefault();
    setData(data => [
      {task: task, isCompleted: false}, ...data]
    );
    setTask("");
  };

  const handleCheck = (index, value) => {
    const newData = [...data];
    newData[index].isCompleted = value;
    setData(newData);
  };

  const handleUpdate = (index, value) => {
    const newData = [...data];
    newData[index].task = value;
    setData(newData);
  };

  const handleDelete = (index) => {
    const currentObject = data.splice(index, 1);
    setData(data => data.filter(object => object != currentObject));
  };

  const handleClear = () => {
    setData(data => data.filter(object => object.isCompleted != true));
  };

  return (
      <div className="container">
        <div className="app">
          <section className="app-header">
            <h1 className="header-title">TO-DO LIST</h1>
          </section>
          <section className="app-body">
            <div className="todo-form">
              <Form onSubmit={handleSubmit}>
                <Form.Control name="todo" className="todo-input" type="text" 
                  onChange={event => setTask(event.target.value)}
                  placeholder="Create a new TO-DO..." 
                  value={task}
                  disabled={edit}
                  autoFocus
                />
                {!data.length && <small className="hint">Hint : Enter to submit.</small>}
              </Form>
            </div>
            {!!data.length && 
              <div className="todo-list">
                <ul className="todo-body">
                  {data.map((data, index) => (
                    <TodoItem key={index}
                      data={data} index={index} 
                      checkHandler={handleCheck}
                      updateHandler={handleUpdate}
                      deleteHandler={handleDelete}
                      editMode={setEdit}
                    />
                  ))}
                </ul>
                <div className="todo-footer">
                  <span className="todo-counter">{data.filter(data => !data.isCompleted).length} to-do left</span>
                  <a className={`todo-clear ${edit ? 'disabled' : ''}`} onClick={handleClear}>Clear Completed</a>
                </div>
              </div>
            }
          </section>
        </div>
      </div>
  );
};

export default App;
