import React, {useState} from 'react';
import { Form } from 'react-bootstrap';
import { IoCheckmarkCircleOutline as Checked, IoEllipseOutline as Unchecked} from "react-icons/io5";
import { HiPencil as Edit, HiX as Close, HiTrash as Delete} from "react-icons/hi";

const TodoItem = ({data, index, checkHandler, updateHandler, deleteHandler, editMode}) =>{
    const [edit, setEdit] = useState(false);
    const [task, setTask] = useState();
    const [check, setCheck] = useState();

    const setEditMode = (value) =>{
        setEdit(value);
        editMode(value);
    }

    const checkBoxHandler = () => {
        if(!edit){
            setCheck(!data.isCompleted); 
            checkHandler(index, !check);
        }
    }

    const deleteBtnHandler = () => {
        if(!edit){
            deleteHandler(index);
            setEditMode(false);
        }
    }

    return (
        <li className="todo-item">
            <div className="item-task">
                <a onClick={checkBoxHandler}>
                    {data.isCompleted ? <Checked className={`checkbox ${edit ? 'disabled' : ''}`}/> : <Unchecked className={`checkbox ${edit ? 'disabled' : ''}`}/>}
                </a>
            </div>
            <div className="item-content">
                {edit ? 
                    <Form onSubmit={(event) => {
                        event.preventDefault();
                        updateHandler(index, task);
                        setEditMode(false);
                    }}>
                        <Form.Control name="todo" className="todo-input" type="text"
                            onChange={event => setTask(event.target.value)}
                            onKeyUp={event => {event.key === "Escape" && setEditMode(false)}}
                            placeholder="Edit a TO-DO..." 
                            defaultValue={data.task}
                            autoFocus
                        />
                    </Form>
                : 
                    data.task
                }
            </div>
            <div className="item-action">
                {edit ? 
                    <Close className="close" onClick={() => setEditMode(false)}/> 
                : 
                    <Edit className="edit" onClick={() => setEditMode(true)}/>
                }
                <Delete className={`delete ${edit ? 'disabled' : ''}`} onClick={deleteBtnHandler}/>
            </div>
        </li>
    )
};

export default TodoItem;